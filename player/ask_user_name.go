package player

import "fmt"

func (user *Player) AskName() {
	fmt.Print("Please, enter your name\nYour name: ")
	fmt.Scan(&user.Name)
}
