package events

import (
	"testing"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

func TestGenerateEvents(t *testing.T) {
	heroForEvents := character.Hero{
		MaxHealth: 100,
		Health:    100,
		Damage:    20,
		Armor:     47,
	}
	dragonForEvents := enemy.Dragon{
		Health:     1000,
		Damage:     5,
		MissChance: 0,
	}

	wantDragonHealth := 6
	dragonForEvents.Health = 7
	GenerateEvents(&dragonForEvents, &heroForEvents, 34, "1")
	if dragonForEvents.Health != wantDragonHealth {
		t.Fatalf("ERROR, dragon Health must be equal %d, but equal %d", wantDragonHealth, dragonForEvents.Health)
	}

	wantDragonHealth = 900
	dragonForEvents.Health = 1000
	GenerateEvents(&dragonForEvents, &heroForEvents, 34, "1")
	if dragonForEvents.Health != wantDragonHealth {
		t.Fatalf("ERROR, dragon Health must be equal %d, but equal %d", wantDragonHealth, dragonForEvents.Health)
	}

	dragonForEvents.Health = 4
	wantDragonHealth = 2
	GenerateEvents(&dragonForEvents, &heroForEvents, 23, "1")
	if dragonForEvents.Health != wantDragonHealth {
		t.Fatalf("ERROR, dragon Health must be equal %d, but equal %d", wantDragonHealth, dragonForEvents.Health)
	}

	dragonForEvents.Health = 1000
	wantDragonHealth = 800
	GenerateEvents(&dragonForEvents, &heroForEvents, 23, "1")
	if dragonForEvents.Health != wantDragonHealth {
		t.Fatalf("ERROR, dragon Health must be equal %d, but equal %d", wantDragonHealth, dragonForEvents.Health)
	}

	dragonForEvents.Health = 8
	wantDragonHealth = 4
	GenerateEvents(&dragonForEvents, &heroForEvents, 29, "1")
	if dragonForEvents.Health != wantDragonHealth {
		t.Fatalf("ERROR, dragon Health must be equal %d, but equal %d", wantDragonHealth, dragonForEvents.Health)
	}

	dragonForEvents.Health = 1000
	wantDragonHealth = 500
	GenerateEvents(&dragonForEvents, &heroForEvents, 29, "1")
	if dragonForEvents.Health != wantDragonHealth {
		t.Fatalf("ERROR, dragon Health must be equal %d, but equal %d", wantDragonHealth, dragonForEvents.Health)
	}

	heroForEvents.Health = 11
	wantHeroHealth := 8
	GenerateEvents(&dragonForEvents, &heroForEvents, 2, "1")
	if heroForEvents.Health != wantHeroHealth {
		t.Fatalf("ERROR, hero Health must be equal %d, but equal %d", wantHeroHealth, heroForEvents.Health)
	}

	wantHeroHealth = 14
	heroForEvents.Health = 20
	GenerateEvents(&dragonForEvents, &heroForEvents, 2, "1")
	if heroForEvents.Health != wantHeroHealth {
		t.Fatalf("ERROR, hero Health must be equal %d, but equal %d", wantHeroHealth, heroForEvents.Damage)
	}

	heroForEvents.Health = 8
	wantHeroHealth = 6
	GenerateEvents(&dragonForEvents, &heroForEvents, 5, "1")
	if heroForEvents.Health != wantHeroHealth {
		t.Fatalf("ERROR, hero Health must be equal %d, but equal %d", wantHeroHealth, heroForEvents.Health)
	}

	wantHeroHealth = 64
	heroForEvents.Health = 80
	GenerateEvents(&dragonForEvents, &heroForEvents, 5, "1")
	if heroForEvents.Health != wantHeroHealth {
		t.Fatalf("ERROR, hero Health must be equal %d, but equal %d", wantHeroHealth, heroForEvents.Health)
	}

}
