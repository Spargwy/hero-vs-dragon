package events

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

//GenerateEvents generate random events for hero or dragon
func GenerateEvents(dragon *enemy.Dragon, hero *character.Hero, chance int, difficulty string) {

	var chanceEventForHero int

	if difficulty == "1" {
		chanceEventForHero = 6
	} else if difficulty == "2" {
		chanceEventForHero = 10
	} else if difficulty == "3" {
		chanceEventForHero = 20
	}

	if chance < chanceEventForHero/2 {
		fmt.Print("An earthquake began, stones fell from the vaults of the cave(\n")
		if hero.Damage > 10 {
			hero.Health = hero.Health - hero.Health/10*3
		} else if hero.Health < 10 {
			hero.Health -= 3
		}
	} else if chance >= chanceEventForHero/2 && chance <= chanceEventForHero {
		fmt.Print("Magnetic storms began, the hero had a headache(reduce damage by 20 percent and -2 wheh hp < 10 \n")
		if hero.Health > 10 {
			hero.Health = hero.Health - hero.Health/10*2
		} else if hero.Health < 10 {
			hero.Health -= 2
		}

	} else if chance > 20 && chance <= 25 { //первые 20 зарезервироввны для героя
		fmt.Print("The cold snap affected the dragon(dragon hp reduced by 20 percent and -2 when hp < 10\n")
		if dragon.Health > 10 {
			dragon.Health = dragon.Health - dragon.Health/10*2
		} else if dragon.Health < 10 {
			dragon.Health -= 2
		}

	} else if chance > 25 && chance <= 30 {
		fmt.Print("You told the dragon that no one loves him. The dragon's Health has decreased by 50 percent and if dragon has smaller than 10hp, reduce = 4.")
		fmt.Print("\nAll is fair in war, but this is too much...\n")
		if dragon.Health > 10 {
			dragon.Health = dragon.Health - dragon.Health/10*5
		} else if dragon.Health < 10 {
			dragon.Health -= 4
		}
	} else if chance > 30 && chance <= 35 {
		fmt.Print("a lot of snow fell and a huge snowball flew into the dragon's mouth(dragon Health reduces by 10 percent and -1 if hp<10\n")
		if dragon.Health > 10 {
			dragon.Health = dragon.Health - dragon.Health/10*1
		} else if dragon.Health < 10 {
			dragon.Health--
		}

	}

}
