FROM golang:1.12
WORKDIR /hero-vs-dragon
COPY . .            
RUN go get ./...

ENV CONN_MYSQL="root:password@/"
ENV CONN_SQLITE="./progress.db"
ENV CONN_PSQL="postgres://postgres:Hero-vs-Dragon1010@localhost/?sslmode=disable"
RUN go build -o hero-vs-dragon

ENTRYPOINT [ "./hero-vs-dragon" ]
