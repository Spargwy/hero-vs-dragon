package enemy

import (
	"math/rand"
	"time"
)

//NameIndex calculate Dragon names array index
func NameIndex(lenght int) int {
	rand.Seed(time.Now().UnixNano())
	nameIndex := rand.Intn(lenght - 1)
	return nameIndex
}
