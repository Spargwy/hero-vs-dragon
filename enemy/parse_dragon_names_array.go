package enemy

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type generatorResponse struct {
	Names []string `json:"names"`
}

//ParseRandomDragonNames array of random dragon names
func ParseRandomDragonNames() ([]string, error) {
	resp, err := http.Get("https://www.namegeneratorfun.com/api/namegenerator?generatorType=list&firstName=&lastName=&minLength=0&maxLength=255&sexId=3&generatorId=73")
	if err != nil {
		fmt.Print(err)
	}

	var outputNames generatorResponse
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return outputNames.Names, err
	}
	dataFromGeneratorSite, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return outputNames.Names, err
	}

	err = json.Unmarshal(dataFromGeneratorSite, &outputNames)
	if err != nil {
		return outputNames.Names, err
	}

	return outputNames.Names, nil
}
