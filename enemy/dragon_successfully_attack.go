package enemy

//DragonSuccessfullyAttack check int chance and return true or false
func DragonSuccessfullyAttack(daragonMissChance, chance int) bool {
	var successfulAttack bool
	if chance >= 100-daragonMissChance {
		successfulAttack = false
	} else {
		successfulAttack = true
	}
	return successfulAttack
}
