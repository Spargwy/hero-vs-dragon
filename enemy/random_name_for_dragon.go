package enemy

//AssignementRandomDragonName вызывает несколько функций, и в итоге присваивает дракону одно из рандомных имён, которые были спарсены с сайта.
func (dragon *Dragon) AssignementRandomDragonName() error {
	arrayOfNames, err := ParseRandomDragonNames()
	if err != nil {
		return err
	}
	dragonNameIndex := NameIndex(len(arrayOfNames))
	dragon.Name = arrayOfNames[dragonNameIndex]
	return nil
}
