package enemy

// Dragon is the main enemy of the game
type Dragon struct {
	Name       string
	Health     int
	Damage     int
	MissChance int
}

func (dragon *Dragon) Init() {
	dragon.Health = 200
	dragon.Damage = 25
	dragon.MissChance = 40
}
