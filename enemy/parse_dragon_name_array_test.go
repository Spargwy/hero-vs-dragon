package enemy

import (
	"fmt"
	"testing"
)

func TestParseRandomDragonNames(t *testing.T) {

	wantArraySize := 10
	dragonNamesArray, err := ParseRandomDragonNames()
	if err != nil {
		t.Error(err)
	}
	arraySize := len(dragonNamesArray)
	if arraySize != wantArraySize {
		fmt.Printf("ERROR!!! Array size must be equal %d, but equal %d ", wantArraySize, arraySize)
	}
	return
}
