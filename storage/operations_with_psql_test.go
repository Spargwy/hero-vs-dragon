package storage

import (
	"fmt"
	"log"
	"testing"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/config"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
	"gitlab.com/Spargwy/hero-vs-dragon/player"
)

func TestCreatePostgreSQL(t *testing.T) {

	var psql PostgreSQL
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES")
	}

	err = psql.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR %v, Tests for postgresql", err)
	}
	err = psql.CreateDB()
	if err != nil {
		log.Fatalf("ERROR %v, Tests for postgresql", err)
	}

	err = psql.CreateTables()
	if err != nil {
		log.Fatalf("ERROR %v, Tests for postgresql", err)
	}
	_, err = psql.db.Exec("DROP TABLE game_progress;")
	if err != nil {
		log.Fatalf("ERROR %v, Tests for postgresql", err)
	}
	err = psql.CreateTables()
	if err != nil {
		log.Fatalf("ERROR %v, Tests for postgresql", err)
	}
	_, err = psql.db.Exec("SELECT USER FROM GAME_PROGRESS")
	if err != nil {
		t.Errorf("Table must contain USER column")
	}
	_, err = psql.db.Exec("SELECT GAME FROM GAME_PROGRESS")
	if err != nil {
		t.Errorf("Table must contain GAME column")
	}
}

func TestInsertPostgreSQL(t *testing.T) {

	psql := PostgreSQL{}
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES")
	}
	err = psql.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR %v, Tests for postgresql", err)
	}
	err = psql.CreateDB()
	if err != nil {
		log.Fatalf("ERROR %v, Tests for postgresql", err)
	}
	err = psql.CreateTables()
	if err != nil {
		log.Fatalf("ERROR %v, Tests for postgresql", err)
	}
	var player player.Player
	player.Name = "dima"
	game := &Game{
		Hero:            character.Hero{},
		Dragon:          enemy.Dragon{},
		Move:            10,
		Player:          player,
		LastMove:        false,
		DifficultyLevel: "",
	}
	psql.Insert(game)

	game, err = psql.Read(player)
	if err != nil {
		log.Fatal(err)
	}

	if game.Move != 10 {
		t.Errorf("ERROR! VALUE IS INCORRECT MUST BE 10 BUT EQUAL %d", game.Move)
	}
}

func TestUpdatePostgreSQL(t *testing.T) {

	player := player.Player{Name: "dima"}
	var psql PostgreSQL
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES")
	}
	err = psql.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR Init%v, Tests for postgresql", err)
	}
	err = psql.CreateDB()
	if err != nil {
		log.Fatalf("ERROR CreateDB%v, Tests for postgresql", err)
	}
	err = psql.CreateTables()
	if err != nil {
		log.Fatalf("ERROR CreateTables%v, Tests for postgresql", err)
	}

	game := &Game{
		Hero:            character.Hero{},
		Dragon:          enemy.Dragon{},
		Move:            20,
		Player:          player,
		LastMove:        false,
		DifficultyLevel: "",
	}
	psql.Insert(game)

	err = psql.Update(game) //Update data in table where player = player.Name
	if err != nil {
		log.Fatalf("ERROR Update%v, Tests for postgresql", err)
	}
	fmt.Print(game.Move)
	game, err = psql.Read(player) //Extract data for check result
	if err != nil {
		log.Fatalf("ERROR Read%v, Tests for postgresql", err)
	}

	if game.Move != 20 {
		t.Errorf("ERROR! Incorrect value %d", game.Move)
	}
	game.Move = 30

	err = psql.Update(game) //update again
	if err != nil {
		log.Fatalf("ERROR Update%v, Tests for postgresql", err)
	}
	game, err = psql.Read(player) //check again
	if err != nil {
		log.Fatalf("ERROR Read%v, Tests for postgresql", err)
	}

	if game.Move != 30 {
		t.Errorf("ERROR! Incorrect value %d", game.Move)
	}
}

func TestDeletePostgreSQL(t *testing.T) {

	player := player.Player{Name: "dima"}
	game := &Game{
		Hero:            character.Hero{},
		Dragon:          enemy.Dragon{},
		Move:            20,
		Player:          player,
		LastMove:        false,
		DifficultyLevel: "",
	}
	psql := PostgreSQL{}

	var checkpointer Checkpointer
	checkpointer = &psql
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES")
	}
	err = psql.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR Init%v, Tests for postgresql", err)
	}
	err = psql.CreateDB()
	if err != nil {
		log.Fatalf("ERROR CreateDB%v, Tests for postgresql", err)
	}
	err = psql.CreateTables()
	if err != nil {
		log.Fatalf("ERROR CreateTables%v, Tests for postgresql", err)
	}
	err = checkpointer.Delete(game)
	if err != nil {
		log.Fatalf("ERROR Delete%v, Tests for postgresql", err)
	}
	game, err = checkpointer.Read(player)
	if game.Move != 0 {
		t.Errorf("ERROR! INCORRECT VALUE %d", game.Move)
	}
}

func TestInitPsql(t *testing.T) {
	psql := PostgreSQL{}
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES")
	}
	err = psql.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR INIT %v, Tests for postgresql", err)
	}
	err = psql.db.Ping()
	if err != nil {
		t.Errorf("Database is not open")
	}
}
