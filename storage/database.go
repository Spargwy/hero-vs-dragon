package storage

import (
	// This line is must for working MySQL database

	_ "github.com/go-sql-driver/mysql" //must for driver work
	"gitlab.com/Spargwy/hero-vs-dragon/player"
)

//Checkpointer declarate methods for operations with checkpoints in game
type Checkpointer interface {
	Insert(*Game) error
	Delete(*Game) error
	Update(*Game) error
	Read(player.Player) (*Game, error)
}
