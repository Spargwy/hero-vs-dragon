package storage

import (
	"database/sql"

	"gitlab.com/Spargwy/hero-vs-dragon/config"
)

//Mysql struct for Mysql database operations
type Mysql struct {
	db *sql.DB
}

//Init - open database
func (psql *Mysql) Init(config *config.ConnectionString) error {

	db, err := sql.Open("mysql", config.Mysql)
	if err != nil {
		return err
	}
	psql.db = db

	return nil
}
