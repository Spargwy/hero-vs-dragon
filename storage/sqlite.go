package storage

import (
	"database/sql"
	"os"

	_ "github.com/mattn/go-sqlite3" //must for driver work
	"gitlab.com/Spargwy/hero-vs-dragon/config"
)

//Sqlite struct for sqlite database operations
type Sqlite struct {
	db *sql.DB
}

//Init - open database
func (sqlite *Sqlite) Init(config *config.ConnectionString) error {
	db, err := sql.Open("sqlite3", config.Sqlite)
	if err != nil {
		return err
	}
	sqlite.db = db
	return nil
}

//CreateDB for sqlite
func (sqlite *Sqlite) CreateDB() error {

	if _, err := os.Stat("./progress.db"); os.IsNotExist(err) {
		_, err := os.Create("progress.db")
		if err != nil {
			return err
		}
	}

	return nil
}
