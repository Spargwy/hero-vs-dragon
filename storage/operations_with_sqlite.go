package storage

import (
	"encoding/json"
)

//CreateTables - create table and columns in db
func (database *Sqlite) CreateTables() error {
	_, err := database.db.Exec("CREATE TABLE IF NOT EXISTS game_progress(GAME JSON, USER STRING)")
	if err != nil {
		return err
	}

	return nil
}

//Insert - insert data into GAME_PROGRESS trough sqlite
func (database *Sqlite) Insert(game *Game) error {
	gameProgress, err := json.Marshal(game)
	if err != nil {
		return err
	}

	_, err = database.db.Exec("INSERT INTO GAME_PROGRESS(USER, GAME) VALUES(?, ?)", game.Player.Name, gameProgress)
	if err != nil {
		return err
	}

	return nil
}

//Update - update columns through sqlite
func (database *Sqlite) Update(game *Game) error {
	gameProgress, err := json.Marshal(game)
	if err != nil {
		return err
	}
	_, err = database.db.Exec("UPDATE GAME_PROGRESS SET GAME = ? WHERE USER = ?", gameProgress, game.Player.Name)
	if err != nil {
		return err
	}

	return nil
}

//Delete delete content of column if game is over through sqlite
func (database *Sqlite) Delete(game *Game) error {

	_, err := database.db.Exec("DELETE FROM GAME_PROGRESS WHERE USER = ?", game.Player.Name)
	if err != nil {
		return err
	}

	return nil
}
