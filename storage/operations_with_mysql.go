package storage

import (
	"encoding/json"
	"log"

	_ "github.com/go-sql-driver/mysql" //import mysql driver
)

//CreateDB - create database
func (database *Mysql) CreateDB() error {
	_, err := database.db.Exec("CREATE DATABASE IF NOT EXISTS progress")
	if err != nil {
		log.Fatalf("ERROR WHILE EXEC CREATE DATABASE COMMAND %v", err)
	}

	_, err = database.db.Exec("USE progress")
	if err != nil {
		log.Fatalf("Error in use DB statement")
	}

	return nil
}

//CreateTables for  mysql DB
func (database *Mysql) CreateTables() error {

	_, err := database.db.Exec("CREATE TABLE IF NOT EXISTS GAME_PROGRESS(GAME JSON, USER VARCHAR(30))")
	if err != nil {
		return err
	}

	return err

}

//Insert insert data in column through mySql
func (database *Mysql) Insert(game *Game) error {

	gameProgress, err := json.Marshal(game)
	if err != nil {
		log.Fatal("error while marhall data")
	}

	_, err = database.db.Exec("INSERT INTO GAME_PROGRESS(USER, GAME) VALUES(?, ?)", game.Player.Name, gameProgress)

	if err != nil {
		log.Fatal("error while insert data")
	}

	return nil
}

//Update edit content in column
func (database *Mysql) Update(game *Game) error {

	gameProgress, err := json.Marshal(game)
	if err != nil {
		return err
	}
	_, err = database.db.Exec("UPDATE GAME_PROGRESS SET GAME = ? WHERE USER = ?", gameProgress, game.Player.Name)
	if err != nil {
		return err
	}

	return nil
}

//Delete if game is over
func (database *Mysql) Delete(game *Game) error {

	_, err := database.db.Exec("DELETE FROM GAME_PROGRESS WHERE USER = ?", game.Player.Name)

	if err != nil {
		return err
	}

	return nil
}
