package storage

import (
	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
	"gitlab.com/Spargwy/hero-vs-dragon/player"
)

//Game contain all important data from the game
type Game struct {
	Hero            character.Hero `json:"hero"`
	Dragon          enemy.Dragon   `json:"dragon"`
	Move            int            `json:"move"`
	Player          player.Player  `json:"player"`
	LastMove        bool           `json:"last_move"`
	DifficultyLevel string         `json:"difficulty"`
}
