package storage

import (
	"fmt"
	"log"
	"testing"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/config"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
	"gitlab.com/Spargwy/hero-vs-dragon/player"
)

func TestCreateMysql(t *testing.T) {

	var mysql Mysql
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES %v TestCreateMysql", err)
	}

	err = mysql.Init(&connectionString)
	if err != nil {
		log.Fatalf("init %v TestCreateMysql", err)
	}
	err = mysql.CreateDB()
	if err != nil {
		log.Fatalf("create DB%v TestCreateMysql", err)
	}

	err = mysql.CreateTables()
	if err != nil {
		log.Fatalf("create tables%v", err)
	}
	_, err = mysql.db.Exec("DROP TABLE GAME_PROGRESS")
	if err != nil {
		log.Fatalf("DROP TABLE %vTestCreateMysql", err)
	}
	err = mysql.CreateTables()
	if err != nil {
		log.Fatalf("Create Table %v", err)
	}
	_, err = mysql.db.Exec("SELECT USER FROM GAME_PROGRESS")
	if err != nil {
		log.Fatalf("Check availability USER column %vTestCreateMysql", err)
	}
	_, err = mysql.db.Exec("SELECT GAME FROM GAME_PROGRESS")
	if err != nil {
		log.Fatalf("Check availability GAME column %v TestCreateMysql", err)
	}

}

func TestInsertMysql(t *testing.T) {

	mysql := Mysql{}

	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES %v TestInsertMysql", err)
	}
	err = mysql.Init(&connectionString)
	if err != nil {
		log.Fatalf("INIT ERROR %v", err)
	}
	var player player.Player
	player.Name = "dima"
	game := &Game{
		Hero:            character.Hero{},
		Dragon:          enemy.Dragon{},
		Move:            10,
		Player:          player,
		LastMove:        false,
		DifficultyLevel: "",
	}

	err = mysql.CreateDB()
	if err != nil {
		log.Fatalf("ERROR CreateDB %v TestInsertMysql", err)
	}
	err = mysql.CreateTables()
	if err != nil {
		t.Error(err)
	}
	err = mysql.Insert(game)
	if err != nil {
		log.Fatalf("ERROR Insert %v TestInsertMysql", err)
	}

	game, err = mysql.Read(player)
	if err != nil {
		log.Fatalf("ERROR Read %v TestInsertMysql", err)
	}

	if game.Move != 10 {
		log.Fatalf("ERROR! VALUE IS INCORRECT MUST BE 10 BUT EQUAL %d", game.Move)
	}
}

func TestUpdateMysql(t *testing.T) {

	player := player.Player{Name: "dima"}
	mysql := Mysql{}

	game := &Game{
		Hero:            character.Hero{},
		Dragon:          enemy.Dragon{},
		Move:            20,
		Player:          player,
		LastMove:        false,
		DifficultyLevel: "",
	}
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES TestUpdateMysql %v TestUpdateMysql", err)
	}
	err = mysql.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR Init %v TestUpdateMysql", err)
	}
	err = mysql.CreateDB()
	if err != nil {
		log.Fatalf("ERROR CreateDB TestUpdateMysql %v TestUpdateMysql", err)
	}

	mysql.Insert(game)

	err = mysql.Update(game) //Update data in table where player = player.Name
	if err != nil {
		log.Fatalf("ERROR Update %v TestUpdateMysql", err)
	}
	fmt.Print(game.Move)
	game, err = mysql.Read(player) //Extract data for check result
	if err != nil {
		log.Fatalf("ERROR Read TestUpdateMysql%v TestUpdateMysql", err)
	}

	if game.Move != 20 {
		t.Errorf("ERROR! Incorrect value %d TestUpdateMysql", game.Move)
	}
	game.Move = 30

	err = mysql.Update(game) //update again
	if err != nil {
		t.Error(err)
	}
	game, err = mysql.Read(player) //check again
	if err != nil {
		log.Fatalf("Read error TestUpdateMysql %v TestUpdateMysql", err)
	}

	if game.Move != 30 {
		t.Errorf("ERROR! Incorrect value %d", game.Move)
	}
}

func TestDeleteMysql(t *testing.T) {

	player := player.Player{Name: "dima"}
	game := &Game{
		Hero:            character.Hero{},
		Dragon:          enemy.Dragon{},
		Move:            20,
		Player:          player,
		LastMove:        false,
		DifficultyLevel: "",
	}
	mysql := Mysql{}

	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES %v TestDeleteMysql", err)
	}
	err = mysql.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR Init %v TestDeleteMysql", err)
	}
	err = mysql.CreateDB()
	if err != nil {
		log.Fatalf("ERROR CreateDB %v TestDeleteMysql", err)
	}
	err = mysql.CreateTables()
	if err != nil {
		log.Fatalf("ERROR Create Tables %v TestDeleteMysql", err)
	}
	err = mysql.Delete(game)
	if err != nil {
		log.Fatalf("ERROR Delete %v TestDeleteMysql", err)
	}
	game, err = mysql.Read(player)
	if game.Move != 0 {
		t.Errorf("ERROR! INCORRECT VALUE %d TestDeleteMysql", game.Move)
	}
}

func TestInitMysql(t *testing.T) {
	mysql := Mysql{}
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES %v", err)
	}
	err = mysql.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR Init %v", err)
	}
	err = mysql.db.Ping()
	if err != nil {
		log.Fatalf("ERROR Database in close mysql\n %v  TestInitMysql", err)
	}
}
