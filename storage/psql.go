package storage

import (
	"database/sql"

	"gitlab.com/Spargwy/hero-vs-dragon/config"
)

//PostgreSQL struct for postgresql database operations
type PostgreSQL struct {
	db *sql.DB
}

//Init - open database
func (psql *PostgreSQL) Init(conn *config.ConnectionString) error {
	db, err := sql.Open("postgres", conn.Psql)
	if err != nil {
		return err
	}
	psql.db = db

	return nil
}
