package storage

import (
	"encoding/json"

	"gitlab.com/Spargwy/hero-vs-dragon/player"
)

//Extract extract data from psql db:D. This realization for postgresql database
func (database PostgreSQL) Read(player player.Player) (game *Game, err error) {
	results, err := database.db.Query("SELECT game FROM game_progress WHERE username = $1", player.Name)
	if err != nil {
		return
	}
	defer results.Close()
	var gameProgress Game

	for results.Next() {
		var gameFromDB []byte
		err = results.Scan(&gameFromDB)
		if err != nil {
			panic(err)
		}
		err = json.Unmarshal(gameFromDB, &gameProgress)
		if err != nil {
			panic(err)
		}
	}

	return &gameProgress, nil
}
