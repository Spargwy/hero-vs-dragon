package storage

import (
	"fmt"
	"log"
	"testing"

	"github.com/kelseyhightower/envconfig"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/config"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
	"gitlab.com/Spargwy/hero-vs-dragon/player"
)

func TestCreateSqlite(t *testing.T) {

	sqlite := Sqlite{}
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES")
	}
	err = sqlite.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR Init%v Tests for sqlite", err)
	}
	err = sqlite.CreateDB()
	if err != nil {
		log.Fatalf("ERROR CreateDB%v Tests for sqlite", err)
	}
	err = sqlite.CreateTables()
	if err != nil {
		log.Fatalf("ERROR CreateTables%v Tests for sqlite", err)
	}

	_, err = sqlite.db.Query("SELECT USER FROM GAME_PROGRESS")
	if err != nil {
		t.Errorf("Table must contain USER column")
	}
	_, err = sqlite.db.Query("SELECT GAME FROM GAME_PROGRESS")
	if err != nil {
		t.Errorf("Table must contain GAME column")
	}

}

func TestInsertSqlite(t *testing.T) {

	sqlite := Sqlite{}

	var player player.Player
	player.Name = "dima"
	game := &Game{
		Hero:            character.Hero{},
		Dragon:          enemy.Dragon{},
		Move:            10,
		Player:          player,
		LastMove:        false,
		DifficultyLevel: "",
	}
	var checkpointer Checkpointer
	checkpointer = &sqlite
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES")
	}
	sqlite.Init(&connectionString)
	checkpointer.Insert(game)
	game, err = checkpointer.Read(player)
	if err != nil {
		log.Fatal(err)
	}

	if game.Move != 10 {
		t.Errorf("ERROR! VALUE IS INCORRECT MUST BE 10 BUT EQUAL %d", game.Move)
	}
}

func TestUpdateSqlite(t *testing.T) {

	player := player.Player{Name: "dima"}
	sqlite := Sqlite{}

	game := &Game{
		Hero:            character.Hero{},
		Dragon:          enemy.Dragon{},
		Move:            20,
		Player:          player,
		LastMove:        false,
		DifficultyLevel: "",
	}
	var checkpointer Checkpointer
	checkpointer = &sqlite
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES")
	}
	err = sqlite.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR Init %v Tests for sqlite", err)
	}
	err = sqlite.CreateDB()
	if err != nil {
		log.Fatalf("ERROR CreateDB %v Tests for sqlite", err)
	}
	err = sqlite.CreateTables()
	if err != nil {
		log.Fatalf("ERROR CreateTables %v Tests for sqlite", err)
	}
	checkpointer.Insert(game)

	err = checkpointer.Update(game) //Update data in table where player = player.Name
	if err != nil {
		log.Fatalf("ERROR Update %v Tests for sqlite", err)
	}

	fmt.Print(game.Move)
	game, err = checkpointer.Read(player) //Extract data for check result
	if err != nil {
		log.Fatalf("ERROR Read %v Tests for sqlite", err)
	}

	if game.Move != 20 {
		t.Fatalf("ERROR! Incorrect value %d", game.Move)
	}
	game.Move = 30

	err = checkpointer.Update(game) //update again
	if err != nil {
		log.Fatalf("ERROR Update %v Tests for sqlite", err)
	}
	game, err = checkpointer.Read(player) //check again
	if err != nil {
		log.Fatalf("ERROR Read %v Tests for sqlite", err)
	}

	if game.Move != 30 {
		t.Fatalf("ERROR! Incorrect value %d", game.Move)
	}
}

func TestDeleteSqlite(t *testing.T) {

	player := player.Player{Name: "dima"}
	game := &Game{
		Hero:            character.Hero{},
		Dragon:          enemy.Dragon{},
		Move:            20,
		Player:          player,
		LastMove:        false,
		DifficultyLevel: "",
	}
	sqlite := Sqlite{}
	var checkpointer Checkpointer
	checkpointer = &sqlite
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES")
	}
	err = sqlite.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR Init%v Tests for sqlite", err)
	}
	err = sqlite.CreateDB()
	if err != nil {
		log.Fatalf("ERROR CreateDB%v Tests for sqlite", err)
	}
	err = sqlite.CreateTables()
	if err != nil {
		log.Fatalf("ERROR CreateTables%v Tests for sqlite", err)
	}
	err = checkpointer.Delete(game)
	if err != nil {
		log.Fatalf("ERROR Delete %v Tests for sqlite", err)
	}
	game, err = checkpointer.Read(player)
	if game.Move != 0 {
		t.Fatalf("ERROR! INCORRECT VALUE %d", game.Move)
	}
}

func TestInitSqlite(t *testing.T) {
	sqlite := Sqlite{}
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES")
	}
	err = sqlite.Init(&connectionString)
	if err != nil {
		log.Fatalf("ERROR INIT %v Tests for sqlite", err)
	}
	err = sqlite.db.Ping()
	if err != nil {
		log.Fatalf("Database is not open %v Tests for sqlite", err)
	}
}
