package storage

import (
	"encoding/json"
	"log"

	"gitlab.com/Spargwy/hero-vs-dragon/player"
)

//Extract using for take params from database
func (database Sqlite) Read(player player.Player) (game *Game, err error) {

	results, err := database.db.Query("SELECT game FROM game_progress WHERE user = ?", player.Name)
	if err != nil {
		return
	}
	defer results.Close()
	var gameProgress Game

	for results.Next() {
		var game []byte
		err = results.Scan(&game)
		if err != nil {
			log.Fatal(err)

		}
		err = json.Unmarshal(game, &gameProgress)
		if err != nil {
			log.Fatal(err)
		}
	}

	return &gameProgress, nil
}
