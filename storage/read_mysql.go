package storage

import (
	"encoding/json"

	"gitlab.com/Spargwy/hero-vs-dragon/player"
)

//Extract using for take params from database
func (database *Mysql) Read(player player.Player) (game *Game, err error) {

	results, err := database.db.Query("SELECT GAME FROM GAME_PROGRESS WHERE USER = ?", player.Name)
	if err != nil {
		return
	}
	defer results.Close()

	var gameProgress Game
	for results.Next() {
		var game []byte
		err = results.Scan(&game)
		if err != nil {
			panic(err)

		}
		err = json.Unmarshal(game, &gameProgress)
		if err != nil {
			panic(err)
		}
	}

	return &gameProgress, nil
}
