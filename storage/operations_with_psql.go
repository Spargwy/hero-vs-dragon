package storage

import (
	"encoding/json"

	_ "github.com/lib/pq" //must for work psql
)

//CreateDB - create table and columns in db
func (database *PostgreSQL) CreateDB() error {

	statement := `SELECT EXISTS(SELECT datname FROM pg_catalog.pg_database WHERE datname = 'progress');`

	row := database.db.QueryRow(statement)
	var exists bool
	err := row.Scan(&exists)
	if err != nil {
		return err
	}

	if exists == false {
		statement = `CREATE DATABASE progress;`
		_, err = database.db.Exec(statement)
		if err != nil {
			return err
		}
	}
	return nil
}

//CreateTables table in database
func (database *PostgreSQL) CreateTables() error {

	_, err := database.db.Query("CREATE TABLE IF NOT EXISTS GAME_PROGRESS(GAME JSON, USERNAME VARCHAR(30))")
	if err != nil {
		return err
	}
	return nil
}

//Insert insert data in column through mySql
func (database *PostgreSQL) Insert(game *Game) error {

	gameProgress, err := json.Marshal(game)
	if err != nil {
		return err
	}
	_, err = database.db.Exec("INSERT INTO game_progress(username, game) VALUES($1, $2)", game.Player.Name, gameProgress)

	if err != nil {
		return err
	}

	return nil
}

//Update edit content in column
func (database *PostgreSQL) Update(game *Game) error {

	gameProgress, err := json.Marshal(game)
	if err != nil {
		return err
	}
	_, err = database.db.Exec("UPDATE game_progress SET game = $1 WHERE username = $2", gameProgress, game.Player.Name)
	if err != nil {
		return err
	}

	return nil
}

//Delete if game is over
func (database *PostgreSQL) Delete(game *Game) error {

	_, err := database.db.Exec("DELETE FROM game_progress WHERE username = $1", game.Player.Name)

	if err != nil {
		return err
	}

	return nil
}
