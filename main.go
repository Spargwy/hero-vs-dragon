package main

import (
	"fmt"
	"log"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/Spargwy/hero-vs-dragon/config"
	"gitlab.com/Spargwy/hero-vs-dragon/game"
	"gitlab.com/Spargwy/hero-vs-dragon/storage"
)

func main() {
	var connectionString config.ConnectionString
	err := envconfig.Process("conn", &connectionString)
	if err != nil {
		log.Fatalf("ERROR TAKE ENV VARIABLES%v\n", err)
	}
	var database storage.Mysql
	fmt.Println(&connectionString)
	err = database.Init(&connectionString) //open database
	if err != nil {
		log.Fatalf("ERROR INIT %v", err)
	}
	err = database.CreateDB()
	if err != nil {
		log.Fatalf("ERROR Create DB %v\n", err)
	}
	err = database.CreateTables()
	if err != nil {
		log.Fatalf("ERROR Create Tables %v\n", err)
	}
	err = game.Run(&database)
	if err != nil {
		log.Fatalf("ERROR GAME RUN %v\n", err)
	}
}
