package config

//ConnectionString for databases
type ConnectionString struct {
	Psql   string
	Mysql  string
	Sqlite string
}
