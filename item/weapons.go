package item

//Weapon is any Weapon from the game
type Weapon struct {
	Name          string
	Damage        int
	MissChance    int
	NumberOfUsing int
	ItemUsed      int
	StartDamage   int
}
