package game

import (
	"math/rand"
	"time"
)

//ChanceForSomeAction calculate seed for successfullyAttack and events function
func ChanceForSomeAction() int {
	rand.Seed(time.Now().UnixNano())
	seed := rand.Intn(100)
	return seed
}
