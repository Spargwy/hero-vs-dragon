package game

import (
	"testing"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/item"
)

func TestIncreaseWeaponDamage(t *testing.T) {
	crossbow := item.Weapon{
		Name:          "crossbow",
		Damage:        30,
		MissChance:    20,
		NumberOfUsing: 100,
		ItemUsed:      0,
	}
	pan := item.Weapon{
		Name:          "pan",
		Damage:        10,
		MissChance:    0,
		NumberOfUsing: 10,
		ItemUsed:      0,
	}
	sword := item.Weapon{
		Name:          "sword",
		Damage:        30,
		MissChance:    20,
		NumberOfUsing: 1,
		ItemUsed:      0,
	}

	Weapons := map[string]*item.Weapon{
		"crossbow": &crossbow,
		"pan":      &pan,
		"sword":    &sword,
	}
	hero := character.Hero{Weapons: Weapons}
	hero.WeaponInUse = &crossbow
	wantCrossbowDamage := 27
	IncreaseWeaponDamage(&hero)
	if hero.Weapons["crossbow"].Damage != wantCrossbowDamage {
		t.Fatalf("ERROR!!! This wapon Damage must be equal %d, but equal %d", wantCrossbowDamage, crossbow.Damage)
	}

	crossbow.Damage = 7
	wantCrossbowDamage = 6
	IncreaseWeaponDamage(&hero)
	if hero.Weapons["crossbow"].Damage != wantCrossbowDamage {
		t.Fatalf("ERROR!!! This wapon Damage must be equal %d, but equal %d", wantCrossbowDamage, crossbow.Damage)
	}

	hero.WeaponInUse = &pan
	wantPanDamage := 9
	IncreaseWeaponDamage(&hero)
	if hero.Weapons["pan"].Damage != wantPanDamage {
		t.Fatalf("ERROR!!! This wapon Damage must be equal %d, but equal %d", wantPanDamage, pan.Damage)
	}

	wantSwordDamage := 30
	IncreaseWeaponDamage(&hero)
	if hero.Weapons["sword"].Damage != wantSwordDamage {
		t.Fatalf("ERROR!!! This wapon Damage must be equal %d, but equal %d", wantSwordDamage, sword.Damage)
	}

}
