package game

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

//HeroAttack is the function that damage dragon
func HeroAttack(hero *character.Hero, dragon *enemy.Dragon, successfulAttack bool) {

	reduceDamage := hero.Tiredness()
	if 0 >= hero.WeaponInUse.Damage-reduceDamage {
		hero.Damage = 0
	} else {
		hero.Damage = hero.WeaponInUse.Damage - reduceDamage
	}
	if hero.WeaponInUse.Damage > 0 {
		if successfulAttack == true {
			dragon.Health = dragon.Health - hero.Damage
			IncreaseWeaponDamage(hero)
		}
	} else if hero.WeaponInUse.Damage <= 0 {
		fmt.Printf("WEAPON IS BROKEN %s \n", hero.WeaponInUse.Name)
		choosedWeapon := ChooseWeapon()
		UsingWeapon(hero, choosedWeapon)
		HeroAttack(hero, dragon, successfulAttack)
	}

}
