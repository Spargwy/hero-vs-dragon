package game

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
)

//IncreaseWeaponDamage increase damage after successfully attack
func IncreaseWeaponDamage(hero *character.Hero) {
	var increaseDamage int
	if hero.WeaponInUse.Damage < 10 {
		increaseDamage = 1
	} else if hero.WeaponInUse.Damage >= 10 {
		increaseDamage = hero.WeaponInUse.Damage / 10
	}
	if hero.WeaponInUse.Name != "sword" { //sword is a standart weapon
		if hero.WeaponInUse.Damage-increaseDamage > 0 {
			hero.WeaponInUse.Damage -= increaseDamage
		} else if hero.WeaponInUse.Damage-increaseDamage <= 0 {
			increaseDamage = (0 - hero.WeaponInUse.Damage) * -1
			hero.WeaponInUse.Damage = hero.WeaponInUse.Damage - increaseDamage
			fmt.Print("This weapon is broke: !", hero.WeaponInUse.Name)
		}
	}
	if hero.WeaponInUse.Name == "crossbow" {
		hero.Weapons["crossbow"].Damage = hero.WeaponInUse.Damage
	} else if hero.WeaponInUse.Name == "pan" {
		hero.Weapons["pan"].Damage = hero.WeaponInUse.Damage
	}
}
