package game

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
	"gitlab.com/Spargwy/hero-vs-dragon/events"
	"gitlab.com/Spargwy/hero-vs-dragon/storage"
)

//Run in the main function and controls the Game progress
func Run(checkpointer storage.Checkpointer) error {

	game, err := gameInit(checkpointer)
	if err != nil {
		return err
	}

	for game.Hero.Health > 0 && game.Dragon.Health > 0 {

		chance := ChanceForSomeAction()
		events.GenerateEvents(&game.Dragon, &game.Hero, chance, game.DifficultyLevel)
		action := AskAboutNextAction()
		ActionExecution(&game.Hero, &game.Dragon, action)
		if game.Dragon.Health > 0 {
			chance := ChanceForSomeAction()
			successfulAttack := enemy.DragonSuccessfullyAttack(game.Dragon.MissChance, chance)
			DragonAttack(&game.Hero, &game.Dragon, successfulAttack)
			OutputMessage(game)
			game.Move++

		} else {
			game.Move++
			game.LastMove = true
			heroWinPicture()
			fmt.Printf("\n\t\t\t\tHERO WINS! THE BATTLE LASTED %d MOVES\n", game.Move)
			checkpointer.Delete(game)

		}
		checkpointer.Update(game)

		if game.Hero.Health <= 0 {
			checkpointer.Delete(game)

		}
	}
	return nil
}
