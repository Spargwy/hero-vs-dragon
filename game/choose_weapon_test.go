package game

import (
	"fmt"
	"testing"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/item"
)

func TestUsingWeapon(t *testing.T) {
	crossbow := item.Weapon{
		Name:          "crossbow",
		Damage:        30,
		MissChance:    20,
		NumberOfUsing: 100,
		ItemUsed:      0,
	}
	pan := item.Weapon{
		Name:          "pan",
		Damage:        10,
		MissChance:    0,
		NumberOfUsing: 10,
		ItemUsed:      9,
	}
	sword := item.Weapon{
		Name:          "sword",
		Damage:        30,
		MissChance:    20,
		NumberOfUsing: 1, //This parameter is not will be increasse because sword is a standart weapon
		ItemUsed:      0,
	}
	weapon := item.Weapon{}
	wantCrossbowName := crossbow.Name
	wantPanName := pan.Name
	wantSwordName := sword.Name

	hero := character.Hero{}
	Weapons := map[string]*item.Weapon{
		"crossbow": &crossbow,
		"pan":      &pan,
		"sword":    &sword,
	}
	hero = character.Hero{Weapons: Weapons}

	UsingWeapon(&hero, "1")
	if hero.WeaponInUse.Name != wantCrossbowName {
		t.Fatalf("Return weapon struct is incorrect!!! Returned %s, but most %s", weapon.Name, wantCrossbowName)
	}
	UsingWeapon(&hero, "2")
	if hero.WeaponInUse.Name != wantPanName {
		t.Fatalf("Return weapon struct is incorrect!!! Returned %s, but most %s", weapon.Name, wantPanName)
	}

	UsingWeapon(&hero, "3")
	if hero.WeaponInUse.Name != wantSwordName {
		t.Fatalf("Return weapon struct is incorrect!!! Returned %s, but most %s", weapon.Name, wantSwordName)
	}
	fmt.Print("OK\n")

}
