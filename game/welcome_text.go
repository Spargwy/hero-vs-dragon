package game

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/storage"
)

//WelcomeText for users with different progress
func WelcomeText(game *storage.Game) {
	if game.Move == 0 {
		fmt.Print("Welcome to the game!\n Your Hero and dragon start the game with next characteristics:\n")
	} else if game.Move > 0 {
		fmt.Print("Welcome back!\n you already have saved progress and continue from it\n")
		fmt.Print("Your Hero and enemy dragon have the next characteristics:\n")
	}
	fmt.Print("\nYour Hero characteristics:\n")
	fmt.Println("  Health:", game.Hero.Health)
	fmt.Println("  Armor:", game.Hero.Armor)
	fmt.Print("\n")
	fmt.Println("You have a weapon with which you can damage the dragon!")
	fmt.Println("Weapon has use limit, so use it with mind! Standart weapon is sword.")
	fmt.Println("It has worst characteristics but hasnt limit and does not break")
	fmt.Print("\n")

	for key := range game.Hero.Weapons {
		fmt.Printf("Weapon Name: %s", game.Hero.Weapons[key].Name)
		fmt.Printf("\n  Damage: %d", game.Hero.Weapons[key].Damage)
		fmt.Printf("\n  Miss chance: %d", game.Hero.Weapons[key].MissChance)
		fmt.Printf("\n  You can use this damage %d times", game.Hero.Weapons[key].NumberOfUsing-game.Hero.Weapons[key].ItemUsed) //если игрок продолжает играть, то оружие уже использовалось и мы выводим оставшееся количество использований
		fmt.Print("\n\n")
	}
	fmt.Print("\nDragon characteristics:\n")
	fmt.Println("  Dragon name:", game.Dragon.Name)
	fmt.Println("  Health:", game.Dragon.Health)
	fmt.Println("  Damage:", game.Dragon.Damage)
	fmt.Print("\n")

	battleStartPicture()
	fmt.Print("\n\n\t\t\t\tTHE BATTLE BEGINS\n\n")

}

func battleStartPicture() {

	s1 := "                                               _   __,----'~~~~~~~~~`-----.__\n"
	s2 := "                                        .  .    `//====-              ____,-'~`\n"
	s3 := "                        -.            /_|// .   /||//  `~~~~`---.___./\n"
	s4 := "                  ______-==.       _-~o  `//    |||  //           _,'`\n"
	s5 := "            __,--'   ,=='||/=_    ;_,_,/ _-'|-   |`/   /        ,'\n"
	s6 := "         _-'      ,='    | //`.    '',/~7  /-   /  ||   `/.     /\n"
	s7 := "       .'       ,'       |  /  /_  '  /  /-   /   ||     /   /\n"
	s8 := "      / _____  /         |      /.`-_/  /|- _/   ,||      // \n"
	s9 := "     ,-'     `-|--'~~`--_ /     `==-/  `| /'--===-'       _/`\n"
	s10 := "              '         `-|      /|    )-'/~'      _,--''/\n"
	s11 := "                          '-~^/_/ |    |   `/_   ,^             //\n"
	s12 := "                                /  /     /__  //~               `/__\n"
	s13 := "                            _,-' _/'/ ,-'~____-'`-/                 ``===/\n"
	s14 := "                          ((->/'    /|||' `.     `/.  ,                _||\n"
	s15 := "             ./                       /_     `/      `~---|__i__i__/--~'_/\n"
	s16 := "            <_n_                     __-^-_    `)  /-.______________,-~'\n"
	s17 := "             `B'+)                  ///,-'~`__--^-  |-------~~~~^'\n"
	s18 := "             /^>                           ///,--~`-/\n"
	s19 := "           `  `                                       -Tua Xiong\n"

	dragonPicture := s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8 + s9 + s10 + s11 + s12 + s13 + s14 + s15 + s16 + s17 + s18 + s19
	fmt.Print(dragonPicture)

}

func dragonWinPicture() {

	s1 := "                                                       ____________\n"
	s2 := "                                 (`-..________....---''  ____..._.-`\n"
	s3 := "                                  \\`._______.._,.---'''     ,'\n"
	s4 := "                                  ; )`.      __..-'`-.      /\n"
	s5 := "                                 / /     _.-' _,.;;._ `-._,'\n"
	s6 := "                                / /   ,-' _.-'  //   ``--._``._\n"
	s7 := "                              ,','_.-' ,-' _.- (( =-    -. `-._`-._____\n"
	s8 := "                            ,;.''__..-'   _..--.\\.--'````--.._``-.`-._`.\n"
	s9 := "             _          |/       ```-'`---'`-...__,._  ``-.`-.`-.`.\n"
	s10 := "  _     _.-,'(__)/__)/-'' `     ___  .          `     /      `--._\n"
	s11 := ",',)---' /|)          `     `      ``-.   `     /     /     `     `-.\n"
	s12 := "/_____--.  '`  `               __..-.  /     . (   < _...-----..._   `.\n"
	s13 := " /_,--..__. \\ .-`./----'';``,..-.__ /  /      ,`_. `.,-'`--'`---''`.  )\n"
	s14 := "           `./`./  `_.-..' ,'   _,-..'  /..,-''(, ,' ; ( _______`___..'__\n"
	s15 := "                   ((,(,__(    ((,(,__,'  ``'-- `'`.(/  `.,..______   SST\n"
	s16 := "                                                      ``--------..._``--.__\n"

	dragonPicture := s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8 + s9 + s10 + s11 + s12 + s13 + s14 + s15 + s16
	fmt.Printf("%s", dragonPicture)
}

func heroWinPicture() {

	s1 := "                                _\n"
	s2 := "                         ==(W{==========-      /===-\n"
	s3 := "                           || (.--.)         /===-_---~~~~~~~----__\n"
	s4 := "                         / | _,|**|,__      |===-~___            _,-'`\n"
	s5 := "             -==//       `/ ' `--'   ),    `\\~//   ~~~~`--._.-~\n"
	s6 := "         ______-==|        /`/_. .__ / /    | |  //          _-~`\n"
	s7 := "   __--~~~  ,-/-==//      (   | .  |~~~~|   | |   `/       ,'\n"
	s8 := "_-~       /'    |  //     )__/==0==-/<>/   / /      /     /\n"
	s9 := " .'        /       |   //      /~/___/~~//  /' /        //   /\n"
	s10 := " /  ____  /         |    /`/.__/-~~   /  |_/'  /          //'\n"
	s11 := " /-'~    ~~~~~---__  |     ~-/~         ( )   /'        _--~`\n"
	s12 := "              /_|      /        _) | ;  ),   __--~~\n"
	s13 := "                '~~--_/      _-~/- |/ /   '-~ /\n"
	s14 := "               {/__--_/}    / //_>-|)<__//      /\n"
	s15 := "               /'   (_/  _-~  | |__>--<__|      |\n"
	s16 := "              |   _/) )-~     | |__>--<__|      |\n"
	s17 := "              / /~ ,_/       / /__>---<__/      |\n"
	s18 := "             o-o _//        /-~_>---<__-~      /\n"
	s19 := "             (^(~          /~_>---<__-      _-~\n"
	s20 := "            ,/|           /__>--<__/     _-~' \n"
	s21 := "         ,//('(          |__>--<__|     /                  .--_\n"
	s22 := "        ( ( '))          |__>--<__|    |                 /' _-_~/\n"
	s23 := "     `-)) )) (           |__>--<__|    | -Tua Xiong    /'  /   ~|`|\n"
	s24 := "    ,/,'//( (             |__>--<_|     |            /'  //      ||\n"
	s25 := "  ,( ( ((, ))              ~-__>--<_~-_  ~--__---~'/'/  /'       VV\n"
	s26 := "`~/  )` ) ,/|                 ~-_~>--<_/-__      __-~ _/\n"
	s27 := "._-~//( )/ )) `                    ~~-'_/_/ /~~~~~__--~\n"
	s28 := " ;'( ')/ ,)(                              ~~~~~~~~\n"
	s29 := "' ') '( (/\n"

	dragonPicture := s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8 + s9 + s10 + s11 + s12 + s13 + s14 + s15 + s16 + s17 + s18 + s19 + s20 + s21 + s22 + s23 + s24 + s25 + s26 + s27 + s28 + s29

	fmt.Print(dragonPicture)
}
