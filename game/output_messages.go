package game

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/storage"
)

//OutputMessage is the function that output messages about hero, dragon health and other parametres
func OutputMessage(game *storage.Game) {

	fmt.Printf("\n\n#++++++++++++++++++++++++++++++++++++\n")
	gameComments(&game.Hero, &game.Dragon)
	fmt.Printf("\n#move: %d\n", game.Move)
	fmt.Printf("#Your Hero's health = %d. \n", game.Hero.Health)
	fmt.Printf("#%s's health = %d\n", game.Dragon.Name, game.Dragon.Health)
	fmt.Printf("#Hero's armor = %d\n", game.Hero.Armor)
	fmt.Printf("#++++++++++++++++++++++++++++++++++++\n\n")

	if game.Hero.Health <= 0 {
		dragonWinPicture()
		fmt.Printf("\n\t\t\t\tDRAGON WINS! THE BATTLE LASTED %d MOVES!\n", game.Move)

	}
}
