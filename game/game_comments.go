package game

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

func gameComments(hero *character.Hero, dragon *enemy.Dragon) string {
	var comment string
	if hero.Health <= dragon.Damage {
		comment = "Hero's health equal" + fmt.Sprint(hero.Health) + "! One more hit and dragon will be win! Maybe it's time to heal?"
	} else if hero.Health > hero.MaxHealth/10*2 && hero.Health <= hero.MaxHealth/10*3 {
		comment = "The hero is weak, but he still has a chance"
	} else if hero.Health > hero.MaxHealth/10*3 && hero.Health <= hero.MaxHealth/10*4 {
		comment = "The dragon inflicted some injuries on the hero, but these are all trifles!"
	} else if hero.Health == hero.MaxHealth {
		comment = "Congratulations! Your hero has maximum health. Now you can fight with new forces!"
	}
	return comment

}
