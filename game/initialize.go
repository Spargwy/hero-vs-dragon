package game

import (
	"log"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
	"gitlab.com/Spargwy/hero-vs-dragon/item"
	"gitlab.com/Spargwy/hero-vs-dragon/player"
	"gitlab.com/Spargwy/hero-vs-dragon/storage"
)

//initialize выполняет первоначальное создание обьктов героя, дракона, оружия. Также присваивает им значения на основе выбранной сложности
func gameInit(checkpointer storage.Checkpointer) (game *storage.Game, err error) {
	//определение структур без значений. Значения будут определены после выбора сложности

	hero := character.Hero{}
	dragon := enemy.Dragon{}
	player := player.Player{}

	crossbow := item.Weapon{}
	pan := item.Weapon{}
	sword := item.Weapon{}

	Weapons := make(map[string]*item.Weapon)
	Weapons["crossbow"] = &crossbow
	Weapons["pan"] = &pan
	Weapons["sword"] = &sword

	hero.Weapons = Weapons
	player.AskName()

	gameFromDB, err := checkpointer.Read(player)
	if err != nil {
		log.Fatal(err)
	}
	if gameFromDB.Dragon.Health > 0 && gameFromDB.Hero.Health > 0 {
		game = gameFromDB
		WelcomeText(game)
		return
	}

	err = dragon.AssignementRandomDragonName()
	if err != nil {
		return
	}
	difficultyLevel := askAboutDifficultyLevel()
	difficultyLevelsParameters(&hero, &dragon, difficultyLevel)

	hero.AskAboutUsingAngryMode()
	hero.AngryMode()
	var move int
	game = &storage.Game{
		Hero:            hero,
		Dragon:          dragon,
		Move:            move,
		LastMove:        false,
		DifficultyLevel: difficultyLevel,
		Player:          player,
	}

	err = checkpointer.Insert(game)
	if err != nil {
		return
	}
	err = checkpointer.Update(game)
	if err != nil {
		return
	}
	WelcomeText(game)

	return game, nil
}
