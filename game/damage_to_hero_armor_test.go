package game

import (
	"testing"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

func TestDamageToArmor(t *testing.T) {
	hero := character.Hero{
		MaxHealth: 100,
		Health:    40,
		Damage:    20,
		Armor:     47,
	}
	hero2 := character.Hero{
		MaxHealth: 100,
		Health:    40,
		Damage:    20,
		Armor:     2,
	}

	hero3 := character.Hero{
		MaxHealth: 100,
		Health:    40,
		Damage:    20,
		Armor:     0,
	}
	dragon := enemy.Dragon{
		Health:     1000,
		Damage:     5,
		MissChance: 0,
	}

	wantHeroHealth := 40
	wantHeroArmor := 42
	damageToArmor(&hero, &dragon)
	if hero.Armor != wantHeroArmor {
		t.Fatalf("ERROR!!! hero Armor must be equal %d, but equal %d", wantHeroArmor, hero.Armor)
	}
	if hero.Health != wantHeroHealth {
		t.Fatalf("ERROR!!! hero Health must be equal %d, but equal %d", wantHeroHealth, hero.Health)
	}

	wantHeroHealth = 37
	wantHeroArmor = 0
	damageToArmor(&hero2, &dragon)
	if hero2.Armor != wantHeroArmor {
		t.Fatalf("ERROR!!! hero Armor must be equal %d, but equal %d", wantHeroArmor, hero2.Armor)
	}
	if hero2.Health != wantHeroHealth {
		t.Fatalf("ERROR!!! hero Health must be equal %d, but equal %d", wantHeroHealth, hero2.Health)
	}

	dragon.Damage = 5
	wantHeroHealth = 35
	wantHeroArmor = 0
	damageToArmor(&hero3, &dragon)
	if hero3.Armor != wantHeroArmor {
		t.Fatalf("ERROR!!! hero Armor must be equal %d, but equal %d", wantHeroArmor, hero3.Armor)
	}
	if hero3.Health != wantHeroHealth {
		t.Fatalf("ERROR!!! hero health must be equal %d, but equal %d", wantHeroHealth, hero3.Health)
	}

}
