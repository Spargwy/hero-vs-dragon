package game

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
)

//ChooseWeapon is the function that want user's input for choose weapon
func ChooseWeapon() string {
	var choosedWeapon string
	fmt.Print("If you want to use Weapon, you can put number of it's Weapon\n")
	fmt.Printf("%s: 1\n", "crossbow")
	fmt.Printf("%s: 2\n", "pan")
	fmt.Printf("%s: 3\n", "sword")
	fmt.Printf("Your choose: ")

	fmt.Scan(&choosedWeapon)
	if choosedWeapon == "\n" || choosedWeapon == "" {
		fmt.Printf("Please, enter the correct value\n")
	}
	return choosedWeapon
}

//UsingWeapon assign weapon to WeaponInUse that was choosen in ChooseWeapon function
func UsingWeapon(hero *character.Hero, choosedWeapon string) {

	switch choosedWeapon {
	case "1":
		if hero.Weapons["crossbow"].ItemUsed < hero.Weapons["crossbow"].NumberOfUsing {
			hero.WeaponInUse = hero.Weapons["crossbow"]
			hero.Weapons["crossbow"].ItemUsed++
		} else {
			fmt.Printf("you have exceeded your Weapon usage limit%d. Please choose another Weapon\n", hero.Weapons["crossbow"].ItemUsed)
			choosedWeapon := ChooseWeapon()
			UsingWeapon(hero, choosedWeapon)
		}
	case "2":
		if hero.Weapons["pan"].ItemUsed < hero.Weapons["pan"].NumberOfUsing {
			hero.WeaponInUse = hero.Weapons["pan"]
			hero.Weapons["pan"].ItemUsed++
		} else {
			fmt.Printf("you have exceeded your Weapon usage limit%d. Please, choose another Weapon\n", hero.Weapons["pan"].ItemUsed)
			choosedWeapon := ChooseWeapon()
			UsingWeapon(hero, choosedWeapon)
		}
	case "3":
		hero.WeaponInUse = hero.Weapons["sword"]

	default:
		fmt.Print("You put the incorrect number, please, enter again\n")
		choosedWeapon := ChooseWeapon()
		UsingWeapon(hero, choosedWeapon)
	}
}
