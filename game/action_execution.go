package game

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

//AskAboutNextAction user about hero's next action
func AskAboutNextAction() string {
	var action string
	fmt.Print("Enter number of the next action\n")
	fmt.Print("attack: 1\n")
	fmt.Print("heal: 2\n")
	fmt.Scan(&action)

	return action
}

//ActionExecution Выполняет действие, которое было выбрано
func ActionExecution(hero *character.Hero, dragon *enemy.Dragon, action string) {

	switch action {
	case "1":
		chooseWeapon := ChooseWeapon()
		UsingWeapon(hero, chooseWeapon)
		chance := ChanceForSomeAction()
		successfulAttack := character.HeroSuccessfullyAttack(hero.WeaponInUse.MissChance, chance)
		HeroAttack(hero, dragon, successfulAttack)
	case "2":
		if hero.Health < hero.MaxHealth {
			hero.Heal()
		} else {
			fmt.Printf("Sorry, but you cant heal your hero, because HP is max. Please, choose another action\n")
			action = AskAboutNextAction()
			ActionExecution(hero, dragon, action)

		}

	default:
		fmt.Printf("Please, choose the correct action\n")
		action = AskAboutNextAction()
		ActionExecution(hero, dragon, action)
	}
}
