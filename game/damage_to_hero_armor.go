package game

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

//Нанесение урона броне, если она имеется. Если броня меньше урона дракона - урон наносится также здоровью героя
func damageToArmor(hero *character.Hero, dragon *enemy.Dragon) {
	damageToArmor := dragon.Damage
	if hero.Armor-damageToArmor > 0 {
		hero.Armor -= damageToArmor
	} else if hero.Armor-damageToArmor < 0 {
		damageToArmor = hero.Armor
		dragon.Damage = dragon.Damage - damageToArmor
		hero.Health = hero.Health - dragon.Damage
		hero.Armor -= damageToArmor
		fmt.Print("Your hero's armor is destroyd! Now, dragon's attack will be reduce your health!")
	}

}
