package game

import (
	"fmt"
	"testing"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

func TestGameComments(t *testing.T) {
	hero := character.Hero{
		MaxHealth: 100,
		Health:    25,
		Damage:    20,
		Armor:     50,
	}
	dragon := enemy.Dragon{
		Health:     1000,
		Damage:     20,
		MissChance: 20,
	}

	hero.Health = 20
	wantComment := "Hero's health equal" + fmt.Sprint(hero.Health) + "! One more hit and dragon will be win! Maybe it's time to heal?"
	comment := gameComments(&hero, &dragon)
	if wantComment != comment {
		t.Fatalf("ERROR!!! Comment must be equal %s, but equal %s", wantComment, comment)
	}
	hero.Health = 29
	wantComment = "The hero is weak, but he still has a chance"
	comment = gameComments(&hero, &dragon)
	if wantComment != comment {
		t.Fatalf("ERROR!!! Comment must be equal %s, but equal %s", wantComment, comment)
	}
	hero.Health = 35
	wantComment = "The dragon inflicted some injuries on the hero, but these are all trifles!"
	comment = gameComments(&hero, &dragon)
	if wantComment != comment {
		t.Fatalf("ERROR!!! Comment must be equal %s, but equal %s", wantComment, comment)
	}

	hero.Health = hero.MaxHealth
	wantComment = "Congratulations! Your hero has maximum health. Now you can fight with new forces!"
	comment = gameComments(&hero, &dragon)
	if wantComment != comment {
		t.Fatalf("ERROR!!! Comment must be equal %s, but equal %s", wantComment, comment)
	}
}
