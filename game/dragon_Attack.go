package game

import (
	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

//DragonAttack functiom that call at the end of the every move
func DragonAttack(hero *character.Hero, dragon *enemy.Dragon, successfulAttack bool) {
	if successfulAttack == true {
		damageToArmor(hero, dragon)
	}
}
