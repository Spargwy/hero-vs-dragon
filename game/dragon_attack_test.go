package game

import (
	"testing"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

type Hero struct {
	MaxHealth int
	Health    int
	Damage    int
}

func TestDragonAttack(t *testing.T) {
	hero := character.Hero{
		MaxHealth: 100,
		Health:    40,
		Damage:    20,
		Armor:     0,
	}
	dragon := enemy.Dragon{
		Health: 1000,
		Damage: 5,
	}
	wantHeroHealth := hero.Health - 5
	DragonAttack(&hero, &dragon, true)
	if hero.Health != wantHeroHealth {
		t.Fatalf("ERROR! Hero health must be euqal %d, but equal %d", wantHeroHealth, hero.Health)
	}

}
