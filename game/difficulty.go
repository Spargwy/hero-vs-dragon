package game

import (
	"fmt"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
)

func askAboutDifficultyLevel() string {
	var difficultyLevel string
	fmt.Print("Please choose the difficult level\nEasy: 1\nMedium: 2\nHard: 3\nYour choose: ")
	fmt.Scan(&difficultyLevel)

	return difficultyLevel
}

func difficultyLevelsParameters(hero *character.Hero, dragon *enemy.Dragon, difficultyLevel string) {
	// Значения, которые не зависят от уровня сложности
	hero.MaxHealth = 100
	hero.Health = 100
	hero.Weapons["crossbow"].Name = "crossbow"
	hero.Weapons["pan"].Name = "pan"
	hero.Weapons["sword"].Name = "sword"

	//Изменение параметров, в зависимости от уровней сложности
	switch difficultyLevel {
	case "1":
		hero.Armor = 100
		dragon.Health = 200
		dragon.Damage = 25
		dragon.MissChance = 40
		hero.Weapons["crossbow"].Damage = 40
		hero.Weapons["crossbow"].MissChance = 20
		hero.Weapons["crossbow"].NumberOfUsing = 10
		hero.Weapons["crossbow"].StartDamage = 30
		hero.Weapons["pan"].Damage = 25
		hero.Weapons["pan"].MissChance = 0
		hero.Weapons["pan"].NumberOfUsing = 20
		hero.Weapons["pan"].StartDamage = 25
		hero.Weapons["sword"].Name = "sword"
		hero.Weapons["sword"].Damage = 25
		hero.Weapons["sword"].MissChance = 15
		hero.Weapons["sword"].StartDamage = 25
		hero.Weapons["sword"].NumberOfUsing = 1 //счетчик использований оружия не будет увеличиваться, т.к. меч - стандартное оружие

	case "2":
		hero.Armor = 50
		dragon.Health = 250
		dragon.Damage = 30
		dragon.MissChance = 40
		hero.Weapons["crossbow"].Damage = 30
		hero.Weapons["crossbow"].MissChance = 25
		hero.Weapons["crossbow"].NumberOfUsing = 7
		hero.Weapons["crossbow"].StartDamage = 30
		hero.Weapons["pan"].Name = "pan"
		hero.Weapons["pan"].Damage = 20
		hero.Weapons["pan"].MissChance = 10
		hero.Weapons["pan"].NumberOfUsing = 15
		hero.Weapons["pan"].StartDamage = 20
		hero.Weapons["sword"].Name = "sword"
		hero.Weapons["sword"].Damage = 20
		hero.Weapons["sword"].MissChance = 20
		hero.Weapons["sword"].StartDamage = 20
		hero.Weapons["sword"].NumberOfUsing = 1

	case "3":
		hero.Armor = 0
		dragon.Health = 300
		dragon.Damage = 40
		dragon.MissChance = 40
		hero.Weapons["crossbow"].Damage = 30
		hero.Weapons["crossbow"].MissChance = 30
		hero.Weapons["crossbow"].NumberOfUsing = 5
		hero.Weapons["crossbow"].StartDamage = 30
		hero.Weapons["pan"].Name = "pan"
		hero.Weapons["pan"].Damage = 15
		hero.Weapons["pan"].MissChance = 15
		hero.Weapons["pan"].StartDamage = 15
		hero.Weapons["pan"].NumberOfUsing = 7
		hero.Weapons["sword"].Name = "sword"
		hero.Weapons["sword"].Damage = 15
		hero.Weapons["sword"].MissChance = 25
		hero.Weapons["sword"].StartDamage = 15
		hero.Weapons["sword"].NumberOfUsing = 1

	default:
		difficultyLevel := askAboutDifficultyLevel()
		difficultyLevelsParameters(hero, dragon, difficultyLevel)

	}
}
