package game

import (
	"testing"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
	"gitlab.com/Spargwy/hero-vs-dragon/item"
)

func TestDifficulty(t *testing.T) {

	wantHero := character.Hero{
		Armor:     100,
		Health:    100,
		MaxHealth: 100,
	}
	wantDragon := enemy.Dragon{
		Health:     200,
		MissChance: 40,
		Damage:     25,
	}
	wantCrossbow := item.Weapon{
		Name:          "crossbow",
		Damage:        40,
		MissChance:    20,
		NumberOfUsing: 10,
		StartDamage:   30,
	}
	wantPan := item.Weapon{
		Name:          "pan",
		Damage:        25,
		MissChance:    0,
		NumberOfUsing: 20,
		StartDamage:   25,
	}
	wantSword := item.Weapon{
		Name:          "sword",
		Damage:        25,
		MissChance:    15,
		NumberOfUsing: 1,
		StartDamage:   25,
	}

	hero := character.Hero{
		Health: 100,
		Armor:  50,
	}
	dragon := enemy.Dragon{
		Health: 100,
	}

	var crossbow, pan, sword item.Weapon
	Weapons := map[string]*item.Weapon{
		"crossbow": &crossbow,
		"pan":      &pan,
		"sword":    &sword,
	}

	hero.Weapons = Weapons
	difficultyLevelsParameters(&hero, &dragon, "1")
	if hero.Armor != wantHero.Armor {
		t.Errorf("ERROR IN CASE 1! Hero must be equal %v, but euqal %v", wantHero, hero)
	}
	if dragon != wantDragon {
		t.Errorf("ERROR IN CASE 1! Dragon must be equal %v, but euqal %v", wantDragon, dragon)
	}
	if crossbow != wantCrossbow {
		t.Errorf("ERROR IN CASE 1! Crossbow must be equal %v, but euqal %v", wantCrossbow, crossbow)
	}
	if pan != wantPan {
		t.Errorf("ERROR IN CASE 1! Pan must be equal %v, but euqal %v", wantPan, pan)
	}
	if sword != wantSword {
		t.Errorf("ERROR IN CASE 1! Sword must be equal %v, but euqal %v", wantSword, sword)
	}

	wantHero.Armor = 50
	wantDragon.Health = 250
	wantDragon.Damage = 30
	wantDragon.MissChance = 40
	wantCrossbow.Damage = 30
	wantCrossbow.MissChance = 25
	wantCrossbow.NumberOfUsing = 7
	wantCrossbow.StartDamage = 30
	wantPan.Damage = 20
	wantPan.MissChance = 10
	wantPan.NumberOfUsing = 15
	wantPan.StartDamage = 20
	wantSword.Name = "sword"
	wantSword.Damage = 20
	wantSword.MissChance = 20
	wantSword.StartDamage = 20
	wantSword.NumberOfUsing = 1

	difficultyLevelsParameters(&hero, &dragon, "2")
	if hero.Armor != wantHero.Armor {
		t.Errorf("ERROR IN CASE 2! Hero must be equal %v, but euqal %v", wantHero, hero)
	}
	if dragon != wantDragon {
		t.Errorf("ERROR IN CASE 2! Dragon must be equal %v, but euqal %v", wantDragon, dragon)
	}
	if crossbow != wantCrossbow {
		t.Errorf("ERROR IN CASE 2! Crossbow must be equal %v, but euqal %v", wantCrossbow, crossbow)
	}
	if pan != wantPan {
		t.Errorf("ERROR IN CASE 2! Pan must be equal %v, but euqal %v", wantPan, pan)
	}
	if sword != wantSword {
		t.Errorf("ERROR IN CASE 2! Sword must be equal %v, but euqal %v", wantSword, sword)
	}

	wantHero.Armor = 0
	wantDragon.Health = 300
	wantDragon.Damage = 40
	wantDragon.MissChance = 40
	wantCrossbow.Name = "crossbow"
	wantCrossbow.Damage = 30
	wantCrossbow.MissChance = 30
	wantCrossbow.NumberOfUsing = 5
	wantCrossbow.StartDamage = 30
	wantPan.Name = "pan"
	wantPan.Damage = 15
	wantPan.MissChance = 15
	wantPan.StartDamage = 15
	wantPan.NumberOfUsing = 7
	wantSword.Name = "sword"
	wantSword.Damage = 15
	wantSword.MissChance = 25
	wantSword.StartDamage = 15
	wantSword.NumberOfUsing = 1

	difficultyLevelsParameters(&hero, &dragon, "3")
	if hero.Armor != wantHero.Armor {
		t.Errorf("ERROR IN CASE 3! Hero must be equal %v, but euqal %v", wantHero, hero)
	}
	if dragon != wantDragon {
		t.Errorf("ERROR IN CASE 3! Dragon must be equal %v, but euqal %v", wantDragon, dragon)
	}
	if crossbow != wantCrossbow {
		t.Errorf("ERROR IN CASE 3! Crossbow must be equal %v, but euqal %v", wantCrossbow, crossbow)
	}
	if pan != wantPan {
		t.Errorf("ERROR IN CASE 3! Pan must be equal %v, but euqal %v", wantPan, pan)
	}
	if sword != wantSword {
		t.Errorf("ERROR IN CASE 3! Sword must be equal %v, but euqal %v", wantSword, sword)
	}
}
