package game

import (
	"fmt"
	"testing"

	"gitlab.com/Spargwy/hero-vs-dragon/character"
	"gitlab.com/Spargwy/hero-vs-dragon/enemy"
	"gitlab.com/Spargwy/hero-vs-dragon/item"
)

func TestHeroAttack(t *testing.T) {

	dragon := enemy.Dragon{
		Health: 1000,
		Damage: 5,
	}
	crossbow := item.Weapon{
		Name:          "crossbow",
		Damage:        30,
		MissChance:    20,
		NumberOfUsing: 100,
		ItemUsed:      0,
		StartDamage:   100,
	}

	pan := item.Weapon{
		Name:          "pan",
		Damage:        10,
		MissChance:    0,
		NumberOfUsing: 10,
		ItemUsed:      0,
	}
	sword := item.Weapon{
		Name:          "sword",
		Damage:        30,
		MissChance:    20,
		NumberOfUsing: 1,
		ItemUsed:      0,
	}

	Weapons := map[string]*item.Weapon{
		"crossbow": &crossbow,
		"pan":      &pan,
		"sword":    &sword,
	}
	hero := character.Hero{
		MaxHealth: 100,
		Health:    100,
		Damage:    100,
		Weapons:   Weapons,
	}

	hero.Weapons = Weapons
	hero.WeaponInUse = &crossbow
	DamageReduction := hero.Tiredness()
	hero.AngryMode()
	wantDragonHealth := dragon.Health - (crossbow.Damage - DamageReduction)
	HeroAttack(&hero, &dragon, true)
	if dragon.Health != wantDragonHealth {
		t.Fatalf("ERROR! Dragon's Health must be equal %d, but equal %d", wantDragonHealth, dragon.Health)
	}
	hero.Health = 30
	dragon.Health = 1000
	DamageReduction = hero.Tiredness()
	wantDragonHealth = dragon.Health - (crossbow.Damage - DamageReduction)
	HeroAttack(&hero, &dragon, true)
	if dragon.Health != wantDragonHealth {
		t.Fatalf("ERROR! Dragon's Health must be equal %d, but equal %d. WEAPON %s, %d, %d", wantDragonHealth, hero.Health, crossbow.Name, crossbow.Damage, crossbow.MissChance)
	}
	hero.Health = 20
	dragon.Health = 1000
	DamageReduction = hero.Tiredness()
	wantDragonHealth = dragon.Health - (crossbow.Damage - DamageReduction)
	HeroAttack(&hero, &dragon, true)
	if dragon.Health != wantDragonHealth {
		t.Fatalf("ERROR! Dragon's Health must be equal %d, but equal %d. WEAPON %s, %d, %d", wantDragonHealth, hero.Health, crossbow.Name, crossbow.Damage, crossbow.MissChance)
	}
	hero.Health = 10
	dragon.Health = 1000
	DamageReduction = hero.Tiredness()
	wantDragonHealth = dragon.Health - (crossbow.Damage - DamageReduction)
	HeroAttack(&hero, &dragon, true)
	if dragon.Health != wantDragonHealth {
		t.Fatalf("ERROR! Dragon's Health must be equal %d, but equal %d. WEAPON %s, %d, %d", wantDragonHealth, hero.Health, crossbow.Name, crossbow.Damage, crossbow.MissChance)
	}
	fmt.Print("OK\n")

}
