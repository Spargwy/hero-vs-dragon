module gitlab.com/Spargwy/hero-vs-dragon

go 1.13

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jmoiron/sqlx v1.3.1 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/julesguesnon/gomon v0.0.0-20191120204920-798f19d6fa7f // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.9.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/miekg/pkcs11 v1.0.3 // indirect
)
