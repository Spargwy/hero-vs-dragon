package character

import (
	"fmt"
	"testing"
)

func TestHeroHeal(t *testing.T) {
	hero := Hero{
		MaxHealth: 100,
		Health:    40,
		Damage:    20,
	}

	healthBeforeHeal := hero.Health
	hero.Heal()
	if healthBeforeHeal >= hero.Health && healthBeforeHeal != hero.MaxHealth {
		t.Fatalf("Health after healing must be bigger than before. health before heal = %d health after heal = %d", healthBeforeHeal, hero.Health)
	}
	fmt.Print("OK\n")

}
