package character

import (
	"testing"

	"gitlab.com/Spargwy/hero-vs-dragon/item"
)

func TestAngryMode(t *testing.T) {

	crossbow := item.Weapon{
		Name:          "crossbow",
		Damage:        30,
		MissChance:    20,
		NumberOfUsing: 100,
		ItemUsed:      0,
		StartDamage:   30,
	}
	pan := item.Weapon{
		Name:          "pan",
		Damage:        10,
		MissChance:    0,
		NumberOfUsing: 100,
		ItemUsed:      0,
		StartDamage:   7,
	}

	Weapons := map[string]*item.Weapon{
		"crossbow": &crossbow,
		"pan":      &pan,
	}
	hero := Hero{Weapons: Weapons, UsingAngryMode: true}

	wantCrossbowDamage := 39
	hero.AngryMode()
	if wantCrossbowDamage != hero.Weapons["crossbow"].Damage {
		t.Errorf("ERROR!!! reduce Damage in the angry mode must be equal %d, but equal %d", wantCrossbowDamage, hero.Weapons["crossbow"].Damage)
	}
	wantPanDamage := 15
	if wantPanDamage != hero.Weapons["pan"].Damage {
		t.Errorf("ERROR!!! reduce Damage in the angry mode must be equal %d, but equal %d", wantPanDamage, hero.Weapons["pan"].Damage)
	}

	hero.Weapons["pan"].Damage = 10
	hero.UsingAngryMode = false
	hero.AngryMode()
	wantPanDamage = 10
	if wantPanDamage != hero.Weapons["pan"].Damage {
		t.Errorf("ERROR!!! reduce Damage in the angry mode must be equal %d, but equal %d", wantPanDamage, hero.Weapons["pan"].Damage)
	}

}
