package character

import (
	"fmt"
	"testing"
)

func TestHeroTiredness(t *testing.T) {
	hero := Hero{
		MaxHealth: 100,
		Health:    100,
		Damage:    20,
	}

	wantDamageReduction := 0
	DamageReduction := hero.Tiredness()
	if wantDamageReduction != DamageReduction {
		t.Fatalf("ERROR!!! Damage reduction must be equal %d, but euqal %d", wantDamageReduction, DamageReduction)
	}
	hero.Health -= 70
	wantDamageReduction = 5
	DamageReduction = hero.Tiredness()
	if wantDamageReduction != DamageReduction {
		t.Fatalf("ERROR!!! Damage reduction must be equal %d, but euqal %d", wantDamageReduction, DamageReduction)
	}
	hero.Health -= 10
	wantDamageReduction = 10
	DamageReduction = hero.Tiredness()
	if wantDamageReduction != DamageReduction {
		t.Fatalf("ERROR!!! Damage reduction must be equal %d, but euqal %d", wantDamageReduction, DamageReduction)
	}
	hero.Health -= 10
	wantDamageReduction = 20
	DamageReduction = hero.Tiredness()
	if wantDamageReduction != DamageReduction {
		t.Fatalf("ERROR!!! Damage reduction must be equal %d, but euqal %d", wantDamageReduction, DamageReduction)

	}
	fmt.Print("OK\n")
}
