package character

//HeroSuccessfullyAttack takes a seed and returns a boolean expression about the success of the attack
func HeroSuccessfullyAttack(weaponMissChance int, chance int) bool {
	var successfulAttack bool
	if chance > 100-weaponMissChance {
		successfulAttack = false
	} else {
		successfulAttack = true
	}

	return successfulAttack
}
