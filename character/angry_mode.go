package character

import "fmt"

// AskAboutUsingAngryMode promts user to use angry mode
func (hero *Hero) AskAboutUsingAngryMode() {
	var useAngryModeOrNo string
	secondTime := false
	if secondTime == false {
		fmt.Print("Do you wan to use angry mode?\nThis mode will increase your damage by 30% from start weapon damage but also increase miss chance!\n Yes: 1\n No: 2\n Your choose: ")
	}
	fmt.Scan(&useAngryModeOrNo)

	if useAngryModeOrNo == "1" {
		hero.UsingAngryMode = true
	} else if useAngryModeOrNo == "2" {
		hero.UsingAngryMode = false
	} else {
		fmt.Print("Please, choose the correct variant")
		secondTime = true
		hero.AskAboutUsingAngryMode()
	}
}

//AngryMode increase hero damage and increase miss chance his weapon
func (hero *Hero) AngryMode() {
	if hero.UsingAngryMode == true {
		for key := range hero.Weapons {
			if hero.Weapons[key].StartDamage >= 20 {
				hero.Weapons[key].Damage += hero.Weapons[key].StartDamage / 10 * 3
				hero.Weapons[key].MissChance += 20

			} else if hero.Weapons[key].StartDamage < 20 {
				hero.Weapons[key].Damage += 5
				hero.Weapons[key].MissChance += 20
			}
		}
	}

}
