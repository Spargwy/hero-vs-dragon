package character

import "fmt"

const maxRegenPoints = 20

// Heal heals the hero!
func (hero *Hero) Heal() {

	hero.Health += 20
	if hero.Health > hero.MaxHealth {
		hero.Health = hero.MaxHealth
	}

	fmt.Printf("You healed your hero!\n")
}
