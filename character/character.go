package character

import "gitlab.com/Spargwy/hero-vs-dragon/item"

// Hero is the main char of the game
type Hero struct {
	MaxHealth      int
	Health         int
	Damage         int
	Armor          int
	WeaponInUse    *item.Weapon
	Weapons        map[string]*item.Weapon
	UsingAngryMode bool
}
