package character

//Tiredness - это усталость героя, которая снижает урон в зависимости от зодровья
func (hero *Hero) Tiredness() int {
	reduceDamage := 0
	if hero.Health <= (hero.MaxHealth/100*30) && hero.Health > hero.MaxHealth/100*20 {
		reduceDamage = 5
	} else if hero.Health <= hero.MaxHealth/100*20 && hero.Health > hero.MaxHealth/100*10 {
		reduceDamage = 10
	} else if hero.Health <= hero.MaxHealth/100*10 {
		reduceDamage = 20
	} else {
		reduceDamage = 0
	}

	return reduceDamage
}
