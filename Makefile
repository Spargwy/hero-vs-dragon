settings:
	docker volume create progress && docker network create hero-vs-dragon

build:
	docker build -t hero-vs-dragon .

run:
	docker run -v progress-db:/hero-vs-dragon --name hero-vs-dragon -it --rm hero-vs-dragon

database-run:
	docker run -d --name database --network hero-vs-dragon --network-alias mysql -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=progress mysql:5.7

database-exec:
	docker exec -it database mysql -p

netshoot:
	docker run -it --network hero-vs-dragon nicolaka/netshoot

runMysql:
	docker-compose run --name hero-vs-dragon --rm hero-vs-dragon && docker-compose run --name mysql --rm mysql